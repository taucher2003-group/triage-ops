# frozen_string_literal: true

require 'yaml'

require_relative './people_set'
require_relative './stage'

module Hierarchy
  class Group < PeopleSet
    def self.raw_data
      # No memoization as it's already cached in MiniCache
      WwwGitLabCom.groups
    end

    def product_managers
      TeamMemberHelper.build_unique_mentions(definition.fetch('product_managers', []))
    end

    def engineering_managers(*scopes)
      keys = engineering_manager_keys(scopes)
      ems = definition.values_at(*keys, []).flatten
      ems = definition.fetch('engineering_managers', []) if ems.empty?

      TeamMemberHelper.build_unique_mentions(ems)
    end

    def section
      stage&.section
    end

    def stage
      @stage ||= Hierarchy::Stage.new(definition['stage']) if Hierarchy::Stage.exist?(definition['stage'])
    end

    def default_labeling
      @default_labeling ||= (definition.dig('triage_ops_config', 'default_labeling') || {}).slice('groups', 'projects')
    end

    def default_labeling_exclude_projects
      @default_labeling_exclude_projects ||= (definition.dig('triage_ops_config', 'default_labeling', 'exclude_projects') || [])
    end

    def to_h(fields = nil)
      super.merge(
        stage: stage,
        default_labeling: default_labeling
      ).compact
    end

    private

    def engineering_manager_keys(scopes)
      scopes.flat_map { |scope| engineering_manager_key_for_scope(scope) }
    end

    def engineering_manager_key_for_scope(scope)
      case scope
      when :all
        %w[engineering_managers backend_engineering_managers frontend_engineering_managers fullstack_managers]
      when :generic
        ['engineering_managers']
      when :backend
        ['backend_engineering_managers']
      when :frontend
        ['frontend_engineering_managers']
      when :fullstack
        ['fullstack_managers']
      else
        raise "Unsupported scope `#{scope.inspect}`. Valid scopes are `:all`, `:generic`, `:backend`, `:frontend`, and `:fullstack`"
      end
    end
  end
end
