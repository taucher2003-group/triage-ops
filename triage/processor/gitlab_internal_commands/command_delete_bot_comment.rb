# frozen_string_literal: true

require_relative '../../triage/event'
require_relative '../../triage/processor'

module Triage
  class CommandDeleteBotComment < Processor
    react_to 'issue.note', 'merge_request.note'

    define_command name: 'delete_bot_comment'

    def applicable?
      event.by_team_member? &&
        command.valid?(event)
    end

    def process
      delete_thread_notes if event.discussion_id
    end

    def documentation
      <<~TEXT
        This processor handles the "@gitlab-bot delete_bot_comment" command to delete bot comments in resources
      TEXT
    end

    private

    def delete_thread_notes
      return unless bot_thread?

      thread_notes.each do |note|
        api_client.delete("#{event.noteable_path}/notes/#{note['id']}")
      end
    end

    def bot_thread?
      thread_notes.first.to_h.dig('author', 'username') == Event::GITLAB_BOT_USERNAME
    end

    def thread_notes
      path = "#{event.noteable_path}/discussions/#{event.discussion_id}/notes"

      api_client.get(path)
    end

    def api_client
      @api_client ||= Triage.api_client
    end
  end
end
