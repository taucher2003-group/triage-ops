# frozen_string_literal: true

require_relative 'community_processor'
require_relative '../../triage/contributing_organization_finder'

module Triage
  class TrackOrganizationContributions < CommunityProcessor
    include Logging

    react_to 'merge_request.update', 'merge_request.merge'
    # The project https://gitlab.com/gitlab-org/developer-relations/contributor-success/track_org_contributions
    TRACK_ORG_CONTRIBUTIONS_PROJECT_ID = 62_760_330

    def applicable?
      wider_community_contribution? &&
        untracked_organizations.any?
    end

    def process
      process_merge_request
    end

    def documentation
      <<~TEXT
        This processor detects contributing organizations to help us build attribution links between users and their orgs.

        The processor posts a comment with the merge request link to the organization's confidential issue.
      TEXT
    end

    private

    def untracked_organizations
      @organizations = contributing_organizations.filter do |org|
        unique_comment(org[:issue_iid]).no_previous_comment?
      end
    end

    def contributing_organizations
      merge_request_contributors.filter_map do |username|
        org_data = ContributingOrganizationFinder.new.find_by_user(username)

        next unless org_data

        {
          issue_iid: contributing_org_issue_iid(org_data),
          name: contributing_org_name(org_data)
        }
      end.uniq
    end

    def merge_request_contributors
      merge_request_assignees << event.resource_author.username
    end

    def merge_request_assignees
      event.payload['assignees'].to_a.map { |assignee| assignee['username'] }
    end

    def process_merge_request
      @organizations.each do |org|
        add_comment_to_issue(org[:issue_iid], org[:name])
      end
    end

    def add_comment_to_issue(org_issue_iid, org_name)
      add_comment(
        unique_comment(org_issue_iid).wrap(comment(org_name)),
        contributing_org_issue_path(org_issue_iid),
        append_source_link: true
      )
    end

    def comment(org_name)
      # appending "+" to the url to render the title of a referenced MR
      <<~MARKDOWN.chomp
         This MR #{event.url}+ has been identified as being contributed by #{org_name}
      MARKDOWN
    end

    def unique_comment(org_issue_iid)
      UniqueComment.new(
        self.class.name,
        event,
        unique_comment_identifier,
        noteable_object_kind: 'issue',
        noteable_resource_iid: org_issue_iid,
        noteable_project_id: TRACK_ORG_CONTRIBUTIONS_PROJECT_ID
      )
    end

    def contributing_org_issue_path(org_issue_iid)
      "/projects/#{TRACK_ORG_CONTRIBUTIONS_PROJECT_ID}/issues/#{org_issue_iid}"
    end

    def contributing_org_issue_iid(organization)
      organization['Issue link'][/\d+$/]
    end

    def contributing_org_name(organization)
      organization['CONTRIBUTOR_ORGANIZATION']
    end

    def unique_comment_identifier
      "#{self.class.name.demodulize} - Project ID #{event.project_id} - MR ID #{event.iid}"
    end
  end
end
