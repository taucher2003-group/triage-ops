# frozen_string_literal: true

require 'digest'

require_relative 'community_processor'
require_relative '../../../lib/merge_request_coach_helper'

module Triage
  class CommandIssueHelp < CommunityProcessor
    include RateLimit
    include MergeRequestCoachHelper

    react_to 'issue.note'
    define_command name: 'help'

    def applicable?
      valid_command?
    end

    def process
      comment = <<~MARKDOWN.chomp.concat("\n")
        Hey there #{coach}, could you please help @#{event.event_actor_username} out?
      MARKDOWN
      append_discussion(comment, append_source_link: true)
    end

    def documentation
      <<~TEXT
        This processor supports the "@gitlab-bot help" triage operation command to ping a merge request coach for help in an issue.
      TEXT
    end

    private

    def cache_key
      @cache_key ||= OpenSSL::Digest.hexdigest('SHA256', "help-commands-sent-#{event.event_actor_id}-#{event.noteable_path}")
    end

    def rate_limit_count
      event.by_team_member? ? 100 : 1
    end

    def rate_limit_period
      3600 # 1 hour
    end
  end
end
