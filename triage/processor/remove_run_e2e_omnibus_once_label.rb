# frozen_string_literal: true

require_relative '../triage/processor'
require_relative '../triage/event'

module Triage
  class RemoveRunE2eOmnibusOnceLabel < Processor
    react_to 'pipeline.success'

    # We won't evaluate an expression if one of the expressions above it are false (important for performance).
    def applicable?
      (event.from_gitlab_org_gitlab? || event.from_gitlab_org_security_gitlab?) &&
        event.merge_request_pipeline? &&
        event.name == 'E2E Omnibus GitLab EE' &&
        event.merge_request.labels.include?(Labels::PIPELINE_RUN_E2E_OMNIBUS_ONCE)
    end

    def process
      remove_labels([Labels::PIPELINE_RUN_E2E_OMNIBUS_ONCE])
    end

    def documentation
      <<~TEXT
        This processor removes the ~"pipeline:run-e2e-omnibus-once" label from a merge request
        when a successful E2E Omnibus pipeline was run for that merge request.
      TEXT
    end

    private

    def remove_labels(label_names)
      comment = "/unlabel #{label_names_to_label_string(label_names)}"
      add_comment(comment, event.merge_request_path, append_source_link: false)
    end

    def label_names_to_label_string(label_names)
      label_names.map { |label| %(~"#{label}") }.join(' ')
    end
  end
end
