# frozen_string_literal: true

require_relative '../triage/processor'
require_relative '../triage/event'
require_relative '../triage/changed_file_list'
require_relative '../../lib/constants/labels'

module Triage
  class CiComponentsLabelProcessor < Processor
    react_to 'merge_request.open', 'merge_request.reopen', 'merge_request.update'

    TEMPLATES_REF_REGEX = %r{^templates/}

    SKIPPED_PROJECTS = [
      Triage::Event::CHART_PROJECT_ID, # https://gitlab.com/gitlab-org/charts/gitlab
      Triage::Event::ASYNC_RETROSPECTIVES_PROJECT_ID, # https://gitlab.com/gitlab-org/async-retrospectives
      Triage::Event::TERRAFORM_PROVIDER_PROJECT_ID # https://gitlab.com/gitlab-org/terraform-provider-gitlab
    ].freeze

    SKIPPED_NAMESPACES = [
      'gitlab-org/professional-services-automation'
    ].freeze

    def applicable?
      return false if skipped_project_or_namespace? || event.label_names.include?(Labels::CI_COMPONENTS_LABEL)

      event.from_gitlab_components? ||
        (event.from_gitlab_org? && mr_contains_templates?)
    end

    def skipped_project_or_namespace?
      SKIPPED_PROJECTS.include?(event.project_id) ||
        SKIPPED_NAMESPACES.any? { |namespace| event.project_path_with_namespace.start_with?(namespace) }
    end

    def process
      add_comment("/label ~#{Labels::CI_COMPONENTS_LABEL}", append_source_link: false)
    end

    def documentation
      <<~TEXT
        This processor adds a `~ci::components` label to MRs with `templates/` folder ref.
      TEXT
    end

    private

    def mr_contains_templates?
      Triage::ChangedFileList.new(event.project_id, event.iid).any_change?(TEMPLATES_REF_REGEX)
    end
  end
end
