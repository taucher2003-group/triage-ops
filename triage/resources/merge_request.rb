# frozen_string_literal: true

require_relative 'milestone'

module Triage
  # rubocop:disable Naming/ConstantName -- I think this is more readable
  # https://docs.gitlab.com/ee/user/project/integrations/webhook_events.html#pipeline-events
  MergeRequestEventAttributes = %i[
    detailed_merge_status
    iid
    source_branch
    target_branch
    target_project_id
    title
    web_url
  ].freeze

  # https://docs.gitlab.com/ee/api/merge_requests.html#get-single-mr
  MergeRequestAPIOnlyAttributes = %i[
    author
    assignees
    reviewers
    labels
    merge_when_pipeline_succeeds
  ].freeze

  MergeRequestAttributes = [
    *MergeRequestEventAttributes,
    *MergeRequestAPIOnlyAttributes
  ].freeze
  # rubocop:enable Naming/ConstantName

  NOT_MERGEABLE_STATUSES = %w[
    not_approved
    approvals_syncing
    blocked_status
    conflict
    draft_status
    not_open
    requested_changes
  ].freeze

  class MergeRequest < Struct.new(*MergeRequestAttributes, keyword_init: true) # rubocop:disable Style/StructInheritance -- Otherwise super doesn't work
    attr_accessor :source_event

    def self.build_from_event(event, ...)
      merge_request = new(...)
      merge_request.source_event = event
      merge_request
    end

    def self.build_from_api(...)
      merge_request = new(...)
      merge_request.source_event = nil
      merge_request
    end

    def initialize(attrs)
      parsed_attrs = attrs.to_h.transform_keys(&:to_sym)

      # merge request attributes from a PipelineEvent payload has a `url` instead of `web_url` key.
      parsed_attrs[:web_url] ||= parsed_attrs[:url] if parsed_attrs[:url]

      super(parsed_attrs.slice(*members))
    end

    def ready_and_approved?
      !NOT_MERGEABLE_STATUSES.include?(detailed_merge_status)
    end

    MergeRequestAPIOnlyAttributes.each do |api_only_attributes|
      define_method(api_only_attributes) do
        repopulate_from_api! if source_event

        super()
      end
    end

    private

    def repopulate_from_api!
      attrs = source_event.api_client.merge_request(target_project_id, iid).to_h

      initialize(attrs)

      self.source_event = nil
    end
  end
end
