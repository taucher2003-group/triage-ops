# frozen_string_literal: true

require_relative 'logging'
require_relative 'sentry'

require 'sucker_punch'

SuckerPunch.logger = Triage::Logging.sucker_punch_logger

SuckerPunch.exception_handler = lambda { |error, klass, args|
  Raven.tags_context(
    job: klass, process: :sucker_punch, service: 'reactive')
  Raven.extra_context(job_args: args)
  Raven.capture_exception(error)

  SuckerPunch.logger.error(error, klass: klass, args: args)
}
