# frozen_string_literal: true

require_relative 'base'

module Triage
  module PipelineFailure
    module Config
      class GitalyUpdateBranch < Base
        SLACK_CHANNEL = 'gitaly-alerts'

        def self.match?(event)
          event.on_instance?(:com) &&
            event.project_path_with_namespace == 'gitlab-org/gitlab' &&
            event.source == 'merge_request_event' &&
            event.ref == 'release-tools/update-gitaly' &&
            event.source_job_id.nil?
        end

        def default_slack_channels
          [SLACK_CHANNEL]
        end
      end
    end
  end
end
