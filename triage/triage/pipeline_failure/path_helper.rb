# frozen_string_literal: true

module Triage
  module PipelineFailure
    module PathHelper
      def incident_discussion_path(config, incident)
        "/projects/#{config.incident_project_id}/issues/#{incident.iid}/discussions"
      end

      def incident_url_pattern(config, incident)
        "projects/#{config.incident_project_id}/issues/#{incident.iid}"
      end
    end
  end
end
