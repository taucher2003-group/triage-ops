# Architecture

[[_TOC_]]

```mermaid
sequenceDiagram
    %% TODO https://mermaid.js.org/syntax/sequenceDiagram.html
    %% - Rack application runs ProcessorJob.perform_async in background
    %%   and immediately responds with ::Rack::Response.new([JSON.dump({ status: :ok })]).finish
    %% - we are not responding with something from the processor at this point
    %% - The request lifecycle stops at Triage App.
    %% - The Handler will run in the background.
    %% - ProcessorJob.perform_async is method from SuckerPunch lib that calls ProcessorJob#perform
    %%   the background - https://github.com/brandonhilkert/sucker_punch/blob/v3.2.0/lib/sucker_punch/job.rb#L38
    %% - https://github.com/brandonhilkert/sucker_punch?tab=readme-ov-file#usage
    %% - SuckerPunch calls Triage::Job#perform which calls Triage::ProcessorJob#execute
    %% - the job rescues StandardError and responds with Rack::Response

    participant Webhook
    participant Service as Rack Web App<br/>(config.ru)
    participant Processor as Triage App

    Webhook->>Service: POST https://triage-ops.gitlab.com/<br/>with JSON payload
    Service->>Processor: Parse JSON and call<br/>ProcessorJob.perform_async
    Processor->>Handler: Handler.new(event).process
    Handler->Processor list: //triage(event)
    loop For each processor
        Processor list-->Handler: Collect processors' results
    end
    Handler->>Processor: Returns<br/>Hash<Processor name,Result object>
    Processor->>Service: Compute a Rack::Response<br/>with a JSON payload
    Service->>Webhook: Return an HTTP response
```
