## What is this label migration for?

_Describe in detail what your migration is for and link to the issue._

%{first_multiline_commit}

## GitLab Duo Workflow Prompt

_If GitLab Duo Workflow assisted with the MR, please include your prompt here for reference._

## Expected impact & dry-runs

_Link to the last successful dry-run for reviewer to validate this label migration._

## Action items

* [ ] Ensure you have read the [label migration handbook page](https://handbook.gitlab.com/handbook/engineering/infrastructure-platforms/developer-experience/development-analytics/create-triage-policy-with-gitlab-duo-workflow-guide)
* (If applicable) Identify the affected groups and how to communicate to them:
  * [ ] /cc @`person_or_group` =>
  * [ ] Relevant Slack channels =>
  * [ ] Engineering week-in-review
* [ ] Close the MR (Don't Merge!) after migration is done.

/label ~"maintenance::workflow" ~"type::maintenance" ~"one-off"
/draft
<!-- template sourced from https://gitlab.com/gitlab-org/quality/triage-ops/-/blob/master/.gitlab/merge_request_templates/One-off-label-migration.md -->
