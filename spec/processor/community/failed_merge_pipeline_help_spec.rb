# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/community/failed_merge_pipeline_help'

RSpec.describe Triage::FailedMergePipelineHelp do
  include_context 'with event', Triage::PipelineEvent do
    let(:event_attrs) do
      {
        status: 'failed',
        payload: payload
      }
    end

    let(:project_id) { 1 }
    let(:merge_request_iid) { 456 }
    let(:event_actor_username) { '@mr_author_username' }
    let(:label_names) { ['frontend', 'Community contribution', 'group::group1'] }
    let(:payload) { JSON.parse(read_fixture('/reactive/merge_train_pipeline_failed.json')) }
  end

  let(:latest_pipeline) { true }
  let(:coach_username) { '@coach_username' }
  let(:reviewers) { [] }

  subject(:failed_merge_pipeline_help) { described_class.new(event) }

  before do
    allow(event).to receive(:latest_pipeline_in_merge_request?).and_return(latest_pipeline)
    allow(failed_merge_pipeline_help).to receive(:select_random_merge_request_coach).and_return(coach_username)
    stub_api_request(
      path: "/projects/#{project_id}/merge_requests/#{merge_request_iid}",
      response_body: {
        'iid' => merge_request_iid,
        'labels' => label_names,
        'assignees' => [{ 'username' => 'mr_author_username' }],
        'reviewers' => reviewers
      }
    )
  end

  include_examples 'registers listeners', ['pipeline.failed']

  describe '#applicable?' do
    include_examples 'event is applicable'

    context 'when MR is not wider community contribution' do
      let(:label_names) { ['backend'] }

      include_examples 'event is not applicable'
    end

    context "when it's not the latest pipeline in MR" do
      let(:latest_pipeline) { false }

      include_examples 'event is not applicable'
    end

    context "when it's not a merge train pipeline" do
      let(:payload) { JSON.parse(read_fixture('/reactive/pipeline_failed.json')) }

      include_examples 'event is not applicable'
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    let(:expected_message) do
      <<~MARKDOWN.chomp
        Hey #{coach_username}! Merge train pipeline failed for this MR.
        Please help to resolve the failure and deliver this contribution, thanks!


        *This message was [generated automatically](https://handbook.gitlab.com/handbook/engineering/infrastructure/engineering-productivity/triage-operations).
        [Improve it](https://gitlab.com/gitlab-org/quality/triage-ops/-/blob/master/triage/processor/community/failed_merge_pipeline_help.rb)
        or [delete it](https://handbook.gitlab.com/handbook/engineering/infrastructure/engineering-productivity/triage-operations/#reactive-delete_bot_comment-command).*
      MARKDOWN
    end

    it 'posts a comment to MR, assigns and pings coach' do
      expect_comment_request(
        noteable_path: "/projects/#{event.project_id}/merge_requests/#{event.merge_request_iid}",
        body: expected_message
      ) do
        failed_merge_pipeline_help.process
      end
    end

    context 'with coach selection' do
      before do
        allow(failed_merge_pipeline_help).to receive(:add_comment)
      end

      context 'when reviewer present' do
        let(:reviewers) { [{ 'username' => 'reviewer_slug' }] }

        it 'selects this reviewer rather instead of searching a new coach' do
          expected_comment = <<~MARKDOWN
            Hey @reviewer_slug! Merge train pipeline failed for this MR.
            Please help to resolve the failure and deliver this contribution, thanks!
          MARKDOWN

          failed_merge_pipeline_help.process

          expect(failed_merge_pipeline_help).not_to have_received(:select_random_merge_request_coach)
          expect(failed_merge_pipeline_help).to have_received(:add_comment).with(
            expected_comment,
            "/projects/#{event.project_id}/merge_requests/#{event.merge_request_iid}",
            append_source_link: true
          )
        end
      end

      context 'when group and role labels present' do
        it 'selects coach with filters' do
          failed_merge_pipeline_help.process

          expect(failed_merge_pipeline_help).to have_received(:select_random_merge_request_coach)
            .with(group: 'Group 1', role: /frontend/)
        end
      end

      context 'when only group label present' do
        let(:label_names) { ['Community contribution', 'backend', 'documentation', 'test::label', 'group::group1'] }

        it 'selects coach with filter' do
          failed_merge_pipeline_help.process

          expect(failed_merge_pipeline_help).to have_received(:select_random_merge_request_coach)
            .with(group: 'Group 1', role: nil)
        end
      end

      context 'when no coach filter labels present' do
        let(:label_names) { ['Community contribution', 'backend', 'documentation', 'test::label'] }

        it 'selects coach without filters' do
          failed_merge_pipeline_help.process

          expect(failed_merge_pipeline_help).to have_received(:select_random_merge_request_coach)
            .with(group: nil, role: nil)
        end
      end
    end
  end
end
