# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/analytics_instrumentation/analytics_instrumentation_experience_feedback'

RSpec.describe Triage::AnalyticsInstrumentationExperienceFeedback do
  include_context 'with event', Triage::MergeRequestEvent do
    let(:event_attrs) do
      {
        object_kind: 'merge_request'
      }
    end
  end

  include_context 'with merge request notes'
  include_examples 'registers listeners', %w[merge_request.merge merge_request.close]

  subject { described_class.new(event) }

  describe '#applicable?' do
    let(:label_names) { [Labels::ANALYTICS_INSTRUMENTATION_REVIEW_PENDING_LABEL, Labels::ANALYTICS_INSTRUMENTATION_APPROVED_LABEL] }

    include_examples 'event is applicable'

    describe 'MR labels' do
      context 'when the merge request is labeled as pending review' do
        let(:label_names) { [Labels::ANALYTICS_INSTRUMENTATION_REVIEW_PENDING_LABEL] }

        include_examples 'event is applicable'
      end

      context 'when the merge request is labeled as approved' do
        let(:label_names) { [Labels::ANALYTICS_INSTRUMENTATION_APPROVED_LABEL] }

        include_examples 'event is applicable'
      end

      context 'when the merge request is not labeled as pending review or approved' do
        let(:label_names) { [] }

        include_examples 'event is not applicable'
      end
    end

    context 'when the author is a bot' do
      let(:event_attrs) do
        {
          object_kind: 'merge_request',
          maybe_automation_author?: true
        }
      end

      include_examples 'event is not applicable'
    end

    context 'when there is already a comment for the same purpose' do
      let(:merge_request_notes) do
        [
          { body: 'review comment 1' },
          { body: comment_mark }
        ]
      end

      include_examples 'event is not applicable'
    end

    context 'when the merge request is labeled as spam' do
      let(:label_names) { %w[Spam] }

      include_examples 'event is not applicable'
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    let(:label_names) { ['group::pipeline security', "devops::stage_with_one_group"] }
    let(:service_ping_dashboard_link) do
      "https://10az.online.tableau.com/#/site/gitlab/views/PDServicePingExplorationDashboard/MetricExplorationbyGroup?Group%20Name=#{event.group_name}&Stage%20Name=#{event.stage_name}"
    end

    it 'posts code review experience message' do
      body = add_automation_suffix do
        <<~MARKDOWN.chomp
          #{subject.__send__(:unique_comment).__send__(:hidden_comment)}
          Hello @#{event.resource_author.username} :wave:

          The Analytics Instrumentation team is actively working on improving the metrics implementation and event tracking processes.
          We would love to hear about your experience and any feedback you might have!

          To provide your feedback, please use [this Google Form](https://forms.gle/PFUTVPWSQqSJ4PYs5).

          Thanks for your help! :heart:

          ---

          - _Looking for existing metrics data?_ Check out the [Metrics Exploration Dashboard for Group:#{event.group_name}](#{service_ping_dashboard_link}).
          - _Need further assistance? Have comments?_
              - Mention `@gitlab-org/analytics-section/analytics-instrumentation/engineers`
              - Reach out in [#g_monitor_analytics_instrumentation](https://gitlab.enterprise.slack.com/archives/CL3A7GFPF) channel on Slack.
        MARKDOWN
      end

      expect_comment_request(event: event, body: body) do
        subject.process
      end
    end
  end
end
