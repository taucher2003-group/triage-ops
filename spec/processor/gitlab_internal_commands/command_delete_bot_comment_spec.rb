# frozen_string_literal: true

require 'spec_helper'
require_relative '../../../triage/triage/event'
require_relative '../../../triage/processor/gitlab_internal_commands/command_delete_bot_comment'

RSpec.describe Triage::CommandDeleteBotComment do
  include_context 'with event', Triage::IssueNoteEvent do
    let(:event_attrs) do
      {
        project_id: project_id,
        new_comment: command_comment,
        by_team_member?: by_team_member,
        payload: {
          'object_attributes' => { 'discussion_id' => discussion_id }
        }
      }
    end

    let(:command_comment)              { '@gitlab-bot delete_bot_comment' }
    let(:by_team_member)               { true }
    let(:discussion_id)                { 1 }
    let(:project_id)                   { 123 }
  end

  subject { described_class.new(event) }

  include_examples 'registers listeners', ['issue.note', 'merge_request.note']

  describe '#applicable?' do
    context 'when command is valid and user is team member' do
      include_examples 'event is applicable'
    end

    context 'when user is not a team member' do
      let(:by_team_member) { false }

      include_examples 'event is not applicable'
    end

    context 'when command is invalid' do
      let(:command_comment) { '@gitlab-bot invalid_command' }

      include_examples 'event is not applicable'
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    let(:thread_notes) do
      [
        { 'id' => 1, 'author' => { 'username' => 'gitlab-bot' } },
        { 'id' => 2, 'author' => { 'username' => 'gitlab-bot' } }
      ]
    end

    let(:api_client) { double('API Client') }

    before do
      allow(Triage).to receive(:api_client).and_return(api_client)
      allow(api_client).to receive(:get).with("#{event.noteable_path}/discussions/#{discussion_id}/notes").and_return(thread_notes)
    end

    context 'when thread is started by gitlab-bot' do
      context 'when the discussion id is not available in payload' do
        let(:discussion_id) { nil }

        it 'does not delete any notes' do
          expect(api_client).not_to receive(:delete)

          subject.process
        end
      end

      context 'when discussion is available' do
        it 'deletes all notes in the thread' do
          thread_notes.each do |note|
            expect(api_client).to receive(:delete).with("#{event.noteable_path}/notes/#{note['id']}")
          end

          subject.process
        end
      end
    end

    context 'when thread is started by another user' do
      let(:thread_notes) do
        [
          { 'id' => 1, 'author' => { 'username' => 'user' } },
          { 'id' => 2, 'author' => { 'username' => 'gitlab-bot' } }
        ]
      end

      it 'does not delete any notes' do
        expect(api_client).not_to receive(:delete)

        subject.process
      end
    end
  end
end
