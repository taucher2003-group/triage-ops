# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../../triage/processor/engineering_productivity/pipeline_health/update_master_pipeline_jobs_status'

RSpec.describe Triage::PipelineHealth::UpdateMasterPipelineJobsStatus do
  include_context 'with event', Triage::PipelineEvent do
    let(:event_attrs) do
      {
        from_gitlab_org_gitlab?: from_gitlab_org_gitlab,
        ref: ref,
        url: 'pipeline_url',
        payload: JSON.parse(read_fixture('/reactive/pipeline_failed.json')),
        source_job_id: source_job_id
      }
    end

    let(:ref) { 'master' }
    let(:from_gitlab_org_gitlab) { true }
    let(:source_job_id) { nil }
  end

  subject(:update_master_pipeline_jobs_status) { described_class.new(event) }

  include_examples 'registers listeners', ['pipeline.failed', 'pipeline.success']

  describe '#applicable?' do
    context 'when event project is not under gitlab-org-gitlab' do
      let(:from_gitlab_org_gitlab) { false }

      include_examples 'event is not applicable'
    end

    context 'when event ref is not master' do
      let(:ref) { 'feature-branch' }

      include_examples 'event is not applicable'
    end

    context 'when it is a downstream pipeline' do
      let(:source_job_id) { '1' }

      include_examples 'event is not applicable'
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    let(:jobs_payload) { [] }

    let(:pipeline_jobs)         { instance_double(Gitlab::PaginatedResponse, auto_paginate: jobs_payload) }
    let(:api_client_double)     { instance_double(Gitlab::Client, pipeline_jobs: pipeline_jobs) }
    let(:status_manager_double) { instance_double(Triage::PipelineJobsStatusManager) }

    before do
      allow(Triage).to receive(:api_client).and_return(api_client_double)
      allow(Triage::PipelineJobsStatusManager).to receive(:new).with(
        api_client: api_client_double,
        project_id: described_class::STATUS_REPO_PROJECT_ID,
        file_name: described_class::PIPELINE_STATUS_FILE,
        branch_name: described_class::STATUS_REPO_BRANCH_NAME
      ).and_return(status_manager_double)
    end

    it 'invokes PipelineJobsStatusManager#update_and_publish' do
      expect(status_manager_double).to receive(:update_and_publish).with(
        jobs_payload: jobs_payload,
        commit_message: "Update #{described_class::PIPELINE_BRANCH_NAME} pipeline status for #{event.url}"
      )

      update_master_pipeline_jobs_status.process
    end
  end
end
