# frozen_string_literal: true

require 'spec_helper'
require_relative '../../triage/processor/remove_run_e2e_omnibus_once_label'

RSpec.describe Triage::RemoveRunE2eOmnibusOnceLabel do
  include_context 'with event', Triage::PipelineEvent do
    let(:project_id)             { Triage::Event::GITLAB_PROJECT_ID }
    let(:iid)                    { 300 }
    let(:merge_request_pipeline) { true }
    let(:pipeline_name)          { 'E2E Omnibus GitLab EE' }

    let(:labels_from_api) { [Labels::PIPELINE_RUN_E2E_OMNIBUS_ONCE] }
    let(:merge_request_from_api) do
      { 'labels' => labels_from_api }
    end

    let(:event_attrs) do
      {
        merge_request_pipeline?: merge_request_pipeline,
        name: pipeline_name,
        object_kind: 'pipeline'
      }
    end

    let(:payload) do
      {
        'project' => {
          'web_url' => 'https://gitlab.com/gitlab-org/gitlab/pipelines/1234'
        },
        'merge_request' => {
          'iid' => iid,
          'target_project_id' => project_id
        }
      }
    end
  end

  before do
    # Request to get the merge request from the API
    stub_api_request(
      path: "/projects/#{project_id}/merge_requests/#{iid}",
      response_body: merge_request_from_api)
  end

  subject { described_class.new(event) }

  include_examples 'registers listeners', ['pipeline.success']
  include_examples 'applicable on contextual event'

  describe '#applicable?' do
    context 'when event project is not under gitlab-org/gitlab nor gitlab-org/security/gitlab' do
      let(:project_id) { '1234' }

      include_examples 'event is not applicable'
    end

    context 'when event project is under gitlab-org/gitlab' do
      let(:project_id) { Triage::Event::GITLAB_PROJECT_ID }

      include_examples 'event is applicable'
    end

    context 'when event project is under gitlab-org/security/gitlab' do
      let(:project_id) { Triage::Event::SECURITY_GITLAB_PROJECT_ID }

      include_examples 'event is applicable'
    end

    context 'when event is not from a merge request pipeline' do
      let(:merge_request_pipeline) { false }

      include_examples 'event is not applicable'
    end

    context 'when event is from a merge request pipeline' do
      let(:merge_request_pipeline) { true }

      include_examples 'event is applicable'
    end

    context 'when the pipeline is not an E2E Omnibus pipeline' do
      let(:pipeline_name) { 'another pipeline' }

      include_examples 'event is not applicable'
    end

    context 'when the pipeline is an E2E Omnibus pipeline' do
      let(:pipeline_name) { 'E2E Omnibus GitLab EE' }

      include_examples 'event is applicable'
    end

    context 'when the merge request for the pipeline does not have the Labels::PIPELINE_RUN_E2E_OMNIBUS_ONCE label' do
      let(:labels_from_api) { [] }

      include_examples 'event is not applicable'
    end

    context 'when the merge request for the pipeline has the Labels::PIPELINE_RUN_E2E_OMNIBUS_ONCE label' do
      let(:labels_from_api) { [Labels::PIPELINE_RUN_E2E_OMNIBUS_ONCE] }

      include_examples 'event is applicable'
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    it 'removes the Labels::PIPELINE_RUN_E2E_OMNIBUS_ONCE label' do
      body = <<~MARKDOWN.chomp
        /unlabel ~"#{Labels::PIPELINE_RUN_E2E_OMNIBUS_ONCE}"
      MARKDOWN

      expect_comment_request(event: event, noteable_path: "/projects/#{project_id}/merge_requests/#{iid}", body: body) do
        subject.process
      end
    end
  end
end
