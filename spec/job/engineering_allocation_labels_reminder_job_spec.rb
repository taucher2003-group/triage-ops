# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/job/engineering_allocation_labels_reminder_job'
require_relative '../../triage/triage/event'

RSpec.describe Triage::EngineeringAllocationLabelsReminderJob do
  include_context 'with event', Triage::IssueEvent do
    let(:event_attrs) do
      {
        object_kind: 'issue',
        project_id: project_id,
        iid: issue_iid,
        event_actor_username: username,
        url: 'https://url'
      }
    end

    let(:project_id) { 12 }
    let(:issue_iid) { 1234 }
    let(:username) { 'requestor' }
    let(:labels) { [] }
  end

  let(:notes) { [] }

  subject(:reminder_job) { described_class.new }

  before do
    stub_api_request(
      path: "/projects/#{project_id}/issues/#{issue_iid}/notes",
      query: { per_page: 100 },
      response_body: notes
    )

    stub_api_request(
      path: "/projects/#{project_id}/issues/#{issue_iid}",
      response_body: {
        project_id: project_id,
        iid: issue_iid,
        labels: labels
      }
    )
  end

  describe '#perform' do
    let(:issue) do
      {
        project_id: project_id,
        iid: issue_iid,
        labels: labels
      }
    end

    shared_examples 'not posting a reminder' do
      it 'does not post a reminder' do
        expect_api_requests do |requests|
          requests << stub_api_request(path: "/projects/#{project_id}/issues/#{issue_iid}", response_body: issue)

          reminder_job.perform(event)
        end
      end
    end

    shared_examples 'posting a reminder to add labels' do
      it 'reminds user to add labels' do
        body = <<~MARKDOWN.chomp
          <!-- triage-serverless EngineeringAllocationLabelsReminder -->
          :wave: `@#{username}`, please ensure the [required labels](https://handbook.gitlab.com/handbook/product/product-processes/#engineering-allocation) are present for ~"Engineering Allocation" [measurements](https://app.periscopedata.com/app/gitlab/912243/Engineering-Allocation):

          - An `~Eng-Consumer::*` label
          - An `~Eng-Producer::*` label
          - A `~priority::*` label
          - A `~severity::*` label when the type is ~"bug"
        MARKDOWN

        expect_api_requests do |requests|
          requests << stub_api_request(path: "/projects/#{project_id}/issues/#{issue_iid}", response_body: issue)
          requests << stub_comment_request(event: event, body: body)

          reminder_job.perform(event)
        end
      end
    end

    context 'when the issue has a previous comment' do
      let(:notes) { [{ body: '<!-- triage-serverless EngineeringAllocationLabelsReminder -->', id: 1234 }] }

      it 'does not post a reminder' do
        expect_no_request { reminder_job.perform(event) }
      end
    end

    context 'when the issue does not have a previous comment' do
      context 'when the issue has all required engineering allocation labels' do
        let(:labels) { %w[Eng-Consumer::Development Eng-Producer::Development priority::1 severity::1] }

        it_behaves_like 'not posting a reminder'
      end

      context 'when the issue does not have all of required engineering allocation labels' do
        let(:labels) { %w[label1] }

        it_behaves_like 'posting a reminder to add labels'
      end

      context 'when the issue does not have Eng-Producer label' do
        let(:labels) { %w[Eng-Consumer::Development priority::1 severity::1] }

        it_behaves_like 'posting a reminder to add labels'
      end

      context 'when the issue does not have Eng-Consumer label' do
        let(:labels) { %w[Eng-Producer::Development priority::1 severity::1] }

        it_behaves_like 'posting a reminder to add labels'
      end

      context 'when the issue does not have priority label' do
        let(:labels) { %w[Eng-Consumer::Development Eng-Producer::Development severity::1] }

        it_behaves_like 'posting a reminder to add labels'
      end

      context 'when the issue is a bug and does not have severity label' do
        let(:labels) { %w[bug Eng-Consumer::Development Eng-Producer::Development priority::1] }

        it_behaves_like 'posting a reminder to add labels'
      end

      context 'when the issue is not a bug and does not have severity label' do
        let(:labels) { %w[Eng-Consumer::Development Eng-Producer::Development priority::1] }

        it_behaves_like 'not posting a reminder'
      end
    end
  end
end
