# frozen_string_literal: true

require 'spec_helper'
require_relative '../../../lib/generate/option'
require_relative '../../../lib/generate/stage_policy'

RSpec.describe Generate::StagePolicy do
  let(:options) { Generate::Option.new(template: template_path, only: only, assign: nil) }
  let(:template_path) { 'path/to/template' }
  let(:only) { nil }
  let(:template_name) { File.basename(template_path) }
  let(:output_dir) { "#{described_class.destination}/#{template_name}" }

  before do
    expect(File)
      .to receive(:read)
      .with(template_path)
      .and_return(<<~ERB)
        <%= stage.key %>
        <%= stage.label %>
        <%= assignees %>
        <%= stage.groups.first.name %>
      ERB

    expect(FileUtils)
      .to receive(:rm_r)

    expect(FileUtils)
      .to receive(:mkdir_p)
      .with(output_dir)
  end

  describe '.run' do
    shared_examples 'generates and writes policy files' do |stages = %w[stage_with_two_groups stage_with_one_group]|
      before do
        allow(File).to receive(:write)
      end

      it 'generates' do
        if stages.include?('stage_with_two_groups')
          expect(File)
            .to receive(:write)
            .with(
              "#{output_dir}/stage_with_two_groups.yml",
              <<~MARKDOWN)
                stage_with_two_groups
                devops::stage with two groups
                ["@fake_be_em", "@fake_em", "@fake_fe_em", "@fake_fs_em", "@fake_pm"]
                Group 1
              MARKDOWN
        end

        if stages.include?('stage_with_one_group')
          expect(File)
            .to receive(:write)
            .with(
              "#{output_dir}/stage_with_one_group.yml",
              <<~MARKDOWN)
                stage_with_one_group
                devops::stage_with_one_group
                []
                Group 3
              MARKDOWN
        end

        described_class.run(options)
      end
    end

    it_behaves_like 'generates and writes policy files'

    context 'when --only is provided with the group' do
      let(:only) { ['stage_with_two_groups'] }

      it_behaves_like 'generates and writes policy files', ['stage_with_two_groups']
    end

    context 'when --only is provided with another group' do
      let(:only) { ['group3'] }

      it 'does not generate anything' do
        expect(File)
          .not_to receive(:write)

        described_class.run(options)
      end
    end
  end
end
