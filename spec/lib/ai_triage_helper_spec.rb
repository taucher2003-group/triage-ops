# frozen_string_literal: true

require 'spec_helper'
require_relative '../../lib/ai_triage_helper'

RSpec.describe AiTriageHelper do
  before do
    allow(AnthropicApi).to receive(:execute).and_return(read_fixture('ai_triage_helper_anthropic_response.json'))
  end

  let(:expected_message) do
    <<~MESSAGE
      Hi @author, thank you for creating this issue!

      We've attempted to automatically add labels to help route your issue to the right team.

      We believe everyone can contribute and welcome you to work on this proposed change, feature or bug fix.
      If you are a new community contributor, start your [onboarding journey](https://contributors.gitlab.com) and get to know our community.

      Team members, please indicate any mistakes by adding `/label ~"automation:ml wrong"`.

      /label ~"automation:ml" ~"Category:Code Review Workflow" ~"group::code review" ~"type::maintenance"
      <details><summary>View AI response JSON</summary>

      ```json
      {
        "category": {
          "existing": null,
          "deduced": "Category:Code Review Workflow",
          "confidence": 80
        },
        "group": {
          "existing": null,
          "deduced": "group::code review",
          "confidence": 90
        },
        "type": {
          "existing": null,
          "deduced": "type::maintenance",
          "confidence": 80
        },
        "subtype": {
          "existing": null,
          "deduced": "maintenance::workflow",
          "confidence": 70
        },
        "areas": {
          "existing": [],
          "deduced": [
            "backend"
          ],
          "confidence": 60
        },
        "note": ""
      }
      ```
      </details>

      ---

      [This message was generated automatically using AI](https://gitlab.com/gitlab-community/quality/triage-ops/-/blob/master/policies/stages/report/untriaged-issues.yml).
      We strive for accuracy and quality, but please note the information may not be error-free.
    MESSAGE
  end

  it 'returns expected message' do
    groups_and_categories = {
      "import_and_integrate" => { categories: %w[api integrations webhooks importers internationalization], project_ids: nil, group_ids: nil },
      "personal_productivity" => { categories: %w[navigation settings notifications], project_ids: [7071551], group_ids: nil }
    }
    message = described_class.new(groups_and_categories).execute('title', 'description', ['label'], 'author')

    expect(message).to eq(expected_message)
  end
end
