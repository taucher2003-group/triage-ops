# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/triage/contributing_organization_finder'

RSpec.describe Triage::ContributingOrganizationFinder, :clean_cache do
  let(:username) { 'username_1' }
  let(:regular_username) { 'regular' }

  describe '#find_by_user' do
    let(:credential_path) { 'path/to/credentials' }
    let(:sheet_id) { '' }
    let(:org1) do
      {
        "CONTRIBUTOR_ORGANIZATION" => "Org 1",
        "CONTRIBUTOR_USERNAMES" => "[ \n  \"username_1\", \n  \"username_2\"\n ]",
        "Issue link" => "http://link_1.com"
      }
    end

    subject(:finder) { described_class.new }

    before do
      stub_env('LEADING_ORGS_TRACKER_SHEET_ID', sheet_id)
      stub_env('GOOGLE_SERVICE_ACCOUNT_CREDENTIALS_PATH', credential_path)
      stub_googleapis_auth_request(credential_path)
      stub_read_mrarr_organization_request
    end

    context 'with GOOGLE_SERVICE_ACCOUNT_CREDENTIALS_PATH env var not defined' do
      it 'warns the variable is not defined and returns nil' do
        stub_env('GOOGLE_SERVICE_ACCOUNT_CREDENTIALS_PATH', '')

        expect(finder).to receive_message_chain(:logger, :warn).with('GOOGLE_SERVICE_ACCOUNT_CREDENTIALS_PATH env variable not defined.')

        expect(finder.find_by_user(username)).to be_nil
      end
    end

    context 'with LEADING_ORGS_TRACKER_SHEET_ID env var not defined' do
      it 'warns the variable is not defined and returns nil' do
        expect(finder).to receive_message_chain(:logger, :warn).with('LEADING_ORGS_TRACKER_SHEET_ID env variable not defined.')

        expect(finder.find_by_user(username)).to be_nil
      end
    end

    context 'with GOOGLE_SERVICE_ACCOUNT_CREDENTIALS_PATH env var defined' do
      let(:sheet_id) { 'SHEET_ID' }

      context 'with valid username' do
        context 'when present in the CSV' do
          it 'returns true' do
            expect(finder.find_by_user(username)).to eq(org1)
          end
        end

        context 'when present in the CSV in different case' do
          let(:username) { 'Username_1' }

          it 'returns true' do
            expect(finder.find_by_user(username)).to eq(org1)
          end
        end

        context 'when not present in the CSV' do
          let(:username) { regular_username }

          it 'returns nil' do
            expect(finder.find_by_user(username)).to be_nil
          end
        end
      end

      context 'when username is nil' do
        let(:username) { nil }

        it 'returns nil' do
          expect(finder.find_by_user(username)).to be_nil
        end
      end
    end
  end
end
