# frozen_string_literal: true

require 'spec_helper'
require_relative '../../../triage/triage/quick_win/label_validator'
require_relative '../../../triage/triage/label_event_finder'
require_relative '../../../triage/triage'
require_relative '../../../triage/triage/event'

RSpec.describe QuickWin::LabelValidator do
  include_context 'with event', Triage::IssueEvent do
    let(:event_attrs) do
      {
        object_kind: 'issue',
        action: 'update',
        weight: 1,
        added_label_names: labels_added,
        description: description,
        from_gitlab_group?: from_gitlab_group
      }
    end
  end

  let(:from_gitlab_group) { true }
  let(:labels_added) { ['quick win'] }
  let(:weight) { 1 }
  let(:description) { 'Default description' }
  let(:test_class) do
    Class.new do
      include QuickWin::LabelValidator

      attr_accessor :resource, :description, :event

      def initialize(resource, description, event)
        @resource = resource
        @description = description
        @event = event
      end
    end
  end

  let(:two_years_ago) do
    Timecop.freeze do
      two_years = (2 * 365 * 24 * 60 * 60)
      Time.now - two_years
    end
  end

  let(:six_months_ago) do
    Timecop.freeze do
      six_months = (6 * 30 * 24 * 60 * 60)
      Time.now - six_months
    end
  end

  let(:resource) do
    {
      project_id: 1,
      iid: 123,
      created_at: two_years_ago
    }
  end

  let(:label_event_finder) { instance_double(Triage::LabelEventFinder) }
  let(:test_instance) { test_class.new(resource, description, event) }

  describe '#quick_win_label_needs_validation?' do
    before do
      allow(test_instance).to receive(:label_event_finder).and_return(label_event_finder)
    end

    context 'when implementation plan is empty' do
      it 'returns true' do
        expect(test_instance.quick_win_label_needs_validation?).to be true
      end
    end

    context 'when implementation plan is not empty' do
      context 'when label was added less than a year ago' do
        before do
          allow(label_event_finder).to receive(:label_added_date).and_return(six_months_ago)
          allow(test_instance).to receive(:has_empty_or_no_implementation_plan?).and_return(false)
        end

        it 'returns false' do
          expect(test_instance.quick_win_label_needs_validation?).to be false
        end
      end

      context 'when label was added more than a year ago' do
        before do
          allow(label_event_finder).to receive(:label_added_date).and_return(two_years_ago)
        end

        it 'returns true' do
          expect(test_instance.quick_win_label_needs_validation?).to be true
        end
      end

      context 'when label_added_date is nil' do
        before do
          allow(label_event_finder).to receive(:label_added_date).and_return(nil)
        end

        context 'when label was added more than a year ago' do
          it 'uses resource created_at date over one year old' do
            expect(test_instance.send(:label_added_date)).to eq(two_years_ago)
          end
        end
      end

      context 'when there is a description' do
        it 'gets the description' do
          expect(test_instance.send(:description)).to eq 'Default description'
        end
      end
    end
  end

  describe '#label_event_finder' do
    it 'initializes LabelEventFinder with correct parameters' do
      label_event_finder_instance = instance_double(Triage::LabelEventFinder)
      expect(Triage::LabelEventFinder).to receive(:new).with({
        project_id: 1,
        resource_iid: 123,
        resource_type: 'issue',
        label_name: 'quick win',
        action: 'add'
      }).and_return(label_event_finder_instance)

      result = test_instance.label_event_finder

      expect(result).to eq(label_event_finder_instance)
    end
  end
end
